import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MdComponent } from './md.component';

export { MdComponent } from './md.component';
export { MdEditorOption, UploadResult, MarkedjsOption } from './md.types';

@NgModule({
  declarations: [
    MdComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    MdComponent
  ]
})
export class MdModule { }
